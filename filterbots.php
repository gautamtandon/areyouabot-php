/* The MIT License (MIT)

Copyright (c) 2013 Gautam Tandon (gt.2001@gmail.com)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

<?php

@session_start();

$timeThreshold = 60; // seconds
$hitThreshold = 5; // number of attempts
$hitsTraceFile = "/tmp/htf.txt"; // hits trace history file

$userAgent = getUserAgent();
$remoteAddr = getRemoteAddr();

if (isset($_REQUEST['validateCaptcha'])) {
	if (isset($_REQUEST['answer']) && isset($_SESSION['captchaAnswer']) && $_REQUEST['answer'] == $_SESSION['captchaAnswer']) {
		unset($_REQUEST['validateCaptcha']);
		$newLinesArr = array();
		if (file_exists($hitsTraceFile)) {
			$lines = file($hitsTraceFile);
			foreach ($lines as $line) {
				$lineArr = str_getcsv($line);
				if ($lineArr[1] != $remoteAddr) {
					array_push($newLinesArr, $line);
				}
			}
		}
		file_put_contents($hitsTraceFile, implode("\n", $newLinesArr));
		header("Location: ".$_SERVER['HTTP_REFERER']);
	} else {
		showCaptcha();
	}
	exit;
} else {
	$now = time();
	$remoteAddrCntr = 0;
	$newLinesArr = array();
	if (file_exists($hitsTraceFile)) {
		$lines = file($hitsTraceFile);
		foreach ($lines as $line) {
			$lineArr = str_getcsv($line);
			if ($lineArr[0] > $now - $timeThreshold) {
				array_push($newLinesArr, $line);
				if ($lineArr[1] == $remoteAddr) {
					$remoteAddrCntr++;
				}
			}
		}
	}
	array_push($newLinesArr, $now.",".$remoteAddr);
	file_put_contents($hitsTraceFile, implode("\n", $newLinesArr));

	if ($userAgent == null || $remoteAddrCntr > $hitThreshold) {
		showCaptcha();
		exit;
	}
}

function getUserAgent() {
	foreach (getallheaders() as $key => $val) {
		if ($key == "User-Agent") {
			return $val;
		}
	}
	return null;
}

function getRemoteAddr() {
	$xFwdedFor = null;
	foreach (getallheaders() as $key => $val) {
		if ($key == "X-Forwarded-For") {
			$xFwdedFor = $val;
			break;
		}
	}
	return $xFwdedFor == null ? $_SERVER['REMOTE_ADDR'] : $xFwdedFor;
}

function showCaptcha() {
	$x = rand(1, 9);
	$y = rand(1, 9);
	$_SESSION['captchaAnswer'] = $x + $y;
	?>
	<html>
		<body>
			<table width="100%" height="100%"><tr valign="middle"><td align="center">
				<form action="" method="post">
					Are you a human? Answer this to prove you are one!<br/>
					<?php echo $x ?> + <?php echo $y ?> = <input type="text" name="answer" value="" />
					<input type="hidden" name="validateCaptcha" value="true" />
					<input type="submit" value="Submit" />
				</form>
			</td></tr></table>
		</body>
	</html>
	<?php
}

?>
Introduction
============
This is a simple "bot checker" utility written in PHP. The current version identifies someone as a bot based on two criteria. First the caller must be a web browser; this is validated by checking User-Agent in the HTTP Request header. And secondly, the caller must not hit the server with over five hits per minute (this is configurable). If any of the above conditions are met, the utility forces the client to go through a CAPTCHA.

Usage
=====
Simply include filterbots.php at the beginning of your web pages. To modify "velocity check" settings, simply update the following variables in the filterbots.php file:

    $timeThreshold = 60; // seconds
    $hitThreshold = 5; // number of attempts

Current version of the utility uses file system to store client IP addresses. You can configure this using the following parameter in the filterbots.php file:

    $hitsTraceFile = "/tmp/htf.txt"; // hits trace history file

Reporting Bugs or Enhancements
==============================
You can log issues or enhancement requests by clicking [here](https://bitbucket.org/gautamtandon/areyouabot-php/issues?status=new&status=open) and 
I will try to address them as soon as possible.
	
License
=======
This utility has been kept open sourced and made available free of cost under the MIT License.

The MIT License (MIT)

Copyright (c) 2013 Gautam Tandon (gt.2001@gmail.com)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
